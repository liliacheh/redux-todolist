import { useState } from "react";
import { useDispatch } from "react-redux";
import {changeStatusTodo, removeTodo} from "../../redux/actions/todo"

export default function TodoItem(props) {
  const [status, setStatus] = useState(props.status);
  const dispatch = useDispatch()
  const changeStatus = () => {
    return status === "done" ? setStatus("undone") : setStatus("done");
  };

  return (
    <div>
      <span
        style={{
          textDecoration: status === "done" ? "line-through" : "none",
        }}
      >
        {props.text}
      </span>
      <button onClick={()=> {changeStatus() 
        dispatch(changeStatusTodo(props.idx))}}>
        mark as {status === "done" ? "undone" : "done"}
      </button>
      <button onClick={()=> dispatch(removeTodo(props.idx))}>remove from list</button>
    </div>
  );
}
