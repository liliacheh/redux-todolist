import { useEffect, useMemo, useState } from "react";
import TodoItem from "../todo-item";
import { useDispatch, useSelector } from "react-redux";
import { addTodo, getTodos, setTodos } from "../../redux/actions/todo";


export default function TodoList() {
  const [isNewTodo, setNewTodo] = useState(false);
  const [inputValue, setInputValue] = useState("");

  const dispatch = useDispatch();
  const todos = useMemo(()=>{
    return [{ idx: 1, text: "Learn React", status: "done" },
  { idx: 2, text: "Learn React Router", status: "todo" },
  { idx: 3, text: "Learn Redux", status: "todo" }]},[])
  const storedTodos = useSelector(state => state.todo)   
  
  useEffect(()=>{
    dispatch(setTodos(todos))
    dispatch(getTodos())
  },[todos,dispatch])
  
 const createTodo = () => {
    return {
        idx: storedTodos.length + 1 ,
        text: inputValue,
        status: "todo"
    }
 }
  return (
    <div>
      <h2>My awesome todo list</h2>
      {storedTodos.map((todo) => (
        <TodoItem key={todo.idx} text={todo.text} 
        status={todo.status} idx={todo.idx}/>
      ))}
      {isNewTodo ? (
        <>
          <input placeholder="insert your todo" value={inputValue} onChange={(e) => setInputValue(e.target.value)}></input>
          <button onClick={() => {dispatch(addTodo(createTodo())) 
            setInputValue('')}}>add</button>
        </>
      ) : (
        <button onClick={() => setNewTodo(true)}>new todo</button>
      )}
    </div>
  );
}
