import { todoTypes } from "../types";

 export function setTodos (todos) {
return {
    type: todoTypes.SET_TODOS,
    payload : todos
}
}
export function getTodos () {
    return {
        type: todoTypes.GET_TODOS,
    }
}
 export function addTodo(todo){
    return{
        type: todoTypes.ADD_TODO,
        payload: todo
    }
 }
 export function changeStatusTodo(idx){
    return {
        type: todoTypes.CHANGE_STATUS_TODO,
        payload: idx
    }
 }
 export function removeTodo(idx){
    return {
        type: todoTypes.REMOVE_TODO,
        payload: idx
    }
 }