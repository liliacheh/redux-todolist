import { todoTypes } from "../types";

const initialState = []

export function todoReducer (state = initialState, action){
    switch (action.type) {
        case todoTypes.SET_TODOS:
            return action.payload;
        case todoTypes.GET_TODOS:
            return state;
        case todoTypes.ADD_TODO:
            return [...state, action.payload];
        case todoTypes.CHANGE_STATUS_TODO:
            const updatedTodos = state.map(todo => {
                if (todo.idx === action.payload) {
                  todo.status = todo.status === "done" ? "todo" : "done";
                }
                return todo;
              });
              return updatedTodos;
        case todoTypes.REMOVE_TODO:
            return state.filter(todo => todo.idx !== action.payload);
            
            default : return state

}}