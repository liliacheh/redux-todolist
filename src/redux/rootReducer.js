// Core
import { todoReducer } from './reducers/todo';
import { combineReducers } from 'redux';

// Reducers

export const rootReducer = combineReducers({
    todo: todoReducer,
});

